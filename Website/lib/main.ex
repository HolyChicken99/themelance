defmodule Client.Pol do
  use Slurp.Client.Handler
  # Default route should always precede other routes

  Slurp.Client.Handler.get("/",
    file: "index.html"
  )

  Slurp.Client.Handler.get("/random", value: random_number)
  Slurp.Client.Handler.get(:default, file: "index.html")

  def random_number() do
    to_string(:rand.uniform(100))
  end
end
