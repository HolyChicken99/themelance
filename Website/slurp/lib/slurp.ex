defmodule Slurp do
  @moduledoc false

  require Logger

  @doc """
    Starts the Server on the given 'port' of the machine
  """
  def start_link(port: port) do
    {:ok, listen_socket} = :gen_tcp.listen(port, active: false, reuseaddr: true)

    Logger.info("Accepting connections on port #{port}")

    {:ok, spawn_link(Slurp, :accept, [listen_socket])}
  end

  @doc false
  def accept(socket) do
    {:ok, request} = :gen_tcp.accept(socket)

    spawn(fn ->
      request
      |> Client.Pol.handle_request()
    end)

    accept(socket)
  end

  @doc false
  def child_spec(opts) do
    %{id: Slurp, start: {Slurp, :start_link, [opts]}}
  end
end
