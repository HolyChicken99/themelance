defmodule Slurp.Utils.Helper do
  def split(url) do
    {String.match?(url, ~r/^www./), Regex.split(~r{/}, url)}
  end

  # 'POST / HTTP/1.1\r\nHost: 127.0.0.1:8080\r\nUser-Agent: curl/7.85.0\r\nAccept: */*\r\nContent-Length: 142\r\nContent-Type: multipart/form-data; boundary=------------------------a853c35dee252f2d\r\n\r\n--------------------------a853c35dee252f2d\r\nContent-Disposition: form-data; name="name"\r\n\r\npol\r\n--------------------------a853c35dee252f2d--\r\n'
  def req_target(string) do
    Enum.at(String.split(string, "\r\n"), 0)
    |> String.split(" ")
    |> Enum.at(1)
  end
end
