#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>
#define assets "/home/lpha/Documents/projects/themelance/Parser/assets"

///\NOTE: Guaranteed that quoted color will be provided

std::string color_extract(std::string &str) {
  int start = str.find('\"');
  int end = start + 1;
  while (str[end] != '\"')
    end++;
  std::string str2 = str.substr(start + 1, end - start - 1);
  return str2;
}

std::string string_to_color(std::string &str) {
  std::ifstream fs;
  fs.open("./color-map");
  std::string str1;
  std::cout << "trying to find " << str << '\n';
  while (std::getline(fs, str1)) {
    if (str1.find(str) != std::string::npos) {
      int pos = str1.find(',');
      return str1.substr(pos + 1, str1.length() - 1);
    }
  }
  return "#000000";
}

void parser(const std::filesystem::directory_entry entry) {
  std::cout << "opening file " << entry.path() << '\n';
  std::vector<std::string> vec{
      "font-lock-builtin-face",  "font-lock-comment-face",
      "font-lock-constant-face", "font-lock-function-name-face",
      "font-lock-keyword-face",  "font-lock-string-face",
      "font-lock-type-face",     "font-lock-variable-name-face"};

  std::ifstream fs;
  fs.open(entry.path().string());
  std::ofstream fs1;
  fs1.open("color_config");
  std::string str;
  while (std::getline(fs, str)) {
    for (auto &x : vec) {
      if (str.find(x) != std::string::npos) {
        fs1 << x << " : " << string_to_color(color_extract(str)) << '\n'
            << "--";
      }
    }
  }
}
int main() {

  for (auto &dir_entry : std::filesystem::directory_iterator{assets}) {
    parser(dir_entry);
  }
  return 0;
}
