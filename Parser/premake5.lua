-- premake5.lua
require "export-compile-commands"
workspace "Parser"
   configurations {  "Release" }
   cppdialect "c++17"
project "Parser"
   kind "ConsoleApp"
   language "C++"
   targetdir "bin/%{cfg.buildcfg}"

   files { "**.h", "**.cpp" }

   filter "configurations:Release"
      defines { "NDEBUG" }
      optimize "On"
